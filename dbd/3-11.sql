/*
* @Author: Andrés Milla
* @Date:   2018-10-22 20:36:43
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-22 20:46:36
*/
SELECT l.Titulo, g.Nombre, e.Denominacion, le.Año_Edicion
FROM LIBRO l INNER JOIN LIBRO-EDITORIAL le ON (l.ISBN = le.ISBN)
             INNER JOIN EDITORIAL e ON (le.Cod_Editorial = e.Cod_Editorial)
             INNER JOIN GENERO g ON (l.Cod_Genero = g.Cod_Genero)
WHERE ( YEAR(le.Año_Edicion) BETWEEN 2010 and 2017 ) AND (l.ISBN NOT IN (
                                                                        SELECT l.ISBN
                                                                        FROM LIBRO l INNER JOIN PRESTAMO p ON (l.ISBN = p. ISBN)
                                                                        )


