/*
* @Author: Andrés
* @Date:   2018-10-20 21:32:45
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:34:49
*/
SELECT ISBN, Titulo, Descripcion
FROM LIBRO l INNER JOIN GENERO g ON l.Cod_Genero = g.Cod_Genero
WHERE g.Nombre = "Drama"
