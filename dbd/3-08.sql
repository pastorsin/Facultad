/*
* @Author: Andrés
* @Date:   2018-10-20 21:51:41
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-22 20:26:17
*/
SELECT s.Apellido, s.Nombre, s.Fecha_Nacimiento, COUNT(p.Cod_Socio) as CANTIDAD_DE_PRESTAMOS
FROM SOCIO s LEFT JOIN PRESTAMO p ON (p.Cod_Socio = s.Cod_Socio)
GROUP BY s.Cod_Socio
HAVING COUNT(p.Cod_Socio) < 15
ORDER BY CANTIDAD_DE_PRESTAMOS, s.Apellido
