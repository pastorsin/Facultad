/*
* @Author: Andrés Milla
* @Date:   2018-10-26 15:28:49
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-26 15:30:58
*/
SELECT c.NombreCine, c.Ubicación
FROM CINE c INNER JOIN FUNCION f ON (f.IdCine = c.IdCine)
GROUP BY f.IdCine, f.IdPelícula, c.NombreCine, c.Ubicación
HAVING COUNT(*) > 6
