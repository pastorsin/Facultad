/*
* @Author: Andrés
* @Date:   2018-10-20 20:38:36
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 20:41:53
*/
SELECT f.nroTicket, f.total, f.fecha, f.hora, c.dni
FROM DETALLE d  INNER JOIN FACTURA f ON (f.nroTicket = d.nroTicket)
                INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
                INNER JOIN CLIENTE c ON (f.idCliente = c.idCliente)
WHERE p.nombreP = "w"
