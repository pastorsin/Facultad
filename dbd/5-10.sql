/*
* @Author: Andrés Milla
* @Date:   2018-10-26 16:17:19
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-26 16:28:55
*/
SELECT  c.NombreCine, COUNT(DISTINCT(f.IdPelicula)) as CANTIDAD
FROM CINE c INNER JOIN FUNCION f (f.IdCine = c.IdCine)
GROUP BY c.IdCine, c.NombreCine
ORDER BY c.NombreCine, CANTIDAD DESC
