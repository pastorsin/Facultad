SELECT a.DNI, a.Apellido, a.Nombre, ac.Calificacion
FROM PROFESOR pr INNER JOIN PROFESOR-CURSO pc ON (pr.DNI = pc.DNI)
                INNER JOIN PERSONA peP ON (pr.DNI = peP.DNI)
                INNER JOIN ALUMNO-CURSO ac ON (ac.Cod_Curso = pc.Cod_Curso)
                INNER JOIN ALUMNO a ON (ac.DNI = a.DNI)
                INNER JOIN PERSONA peA ON (peA.DNI = a.DNI)
WHERE ac.Calificacion < 6 and pr.Nombre = "Natalio" and pr.Apellido = "Zapata"
ORDER BY a.Apellido
