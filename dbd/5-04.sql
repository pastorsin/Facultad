/*
* @Author: Andrés Milla
* @Date:   2018-10-25 22:12:52
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 22:33:59
*/
(SELECT p.Título, p.Calificación, p.Sinopsis
FROM ACTUACION a INNER JOIN PELICULA p (a.IdPelicula = p.IdPelicula)
                 INNER JOIN ACTORDIRECTOR ad (a.IdAD = ad.IdAD)
WHERE ad.NombreAD = "Vin Diesel")

INTERSECT

(SELECT p.Título, p.Calificación, p.Sinopsis
FROM DIRECCION d INNER JOIN PELICULA p (d.IdPelicula = p.IdPelicula)
                 INNER JOIN ACTORDIRECTOR ad (d.IdAD = ad.IdAD)
WHERE ad.NombreAD LIKE "Alfred%")

ORDER BY p.Titulo

