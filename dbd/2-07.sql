/*
* @Author: Andrés
* @Date:   2018-10-20 20:49:33
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 20:57:08
*/
SELECT c.nombre, c.apellido, c.dni, c.telefono, c.direccion
FROM DETALLE d  INNER JOIN FACTURA f  ON (d.nroTicket = f.nroTicket)
                INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
                INNER JOIN CLIENTE c  ON (f.idCliente = c.idCliente)
WHERE YEAR(f.fecha) = 2017
