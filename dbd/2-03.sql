/*
* @Author: Andrés
* @Date:   2018-10-20 18:26:13
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 18:46:14
*/
SELECT p.nombreP as NOMBRE, COUNT(d.idProducto) as VENTAS
FROM PRODUCTO p LEFT JOIN DETALLE d ON (p.idProducto = d.idProducto)
GROUP BY p.idProducto, p.nombreP
