/*
* @Author: Andrés Milla
* @Date:   2018-10-25 22:45:41
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-26 15:28:05
*/
SELECT p.Título
FROM PELICULA p
WHERE NOT EXISTS (SELECT *
                  FROM ZONA z
                  WHERE NOT EXISTS (SELECT *
                                    FROM FUNCION f  INNER JOIN CINE c ON (f.IdCine = c.IdCine)
                                    WHERE (z.IdZona = c.IdZona and f.IdPelicula = p.IdPelicula)
                                   )
                  )
