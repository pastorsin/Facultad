/*
* @Author: Andrés Milla
* @Date:   2018-10-24 16:03:55
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 16:13:24
*/
-- Que pasa si hay más de un curso?
SELECT c.Nombre, MAX(CANTIDAD) as CANTIDAD, 'Maximo' as DESCRIPCION
FROM (SELECT c.Nombre, COUNT(*) as CANTIDAD
      FROM ALUMNO-CURSO ac INNER JOIN ALUMNO a ON (ac.DNI = a.DNI)
                           INNER JOIN CURSO c ON (c.Cod_Curso = ac.Cod_Curso)
      WHERE ac.Año = 2016
      GROUP BY ac.Cod_Curso)

UNION

SELECT c.Nombre, MIN(CANTIDAD) as CANTIDAD, 'Minimo' as DESCRIPCION
FROM (SELECT c.Nombre, COUNT(*) as CANTIDAD
      FROM ALUMNO-CURSO ac INNER JOIN ALUMNO a ON (ac.DNI = a.DNI)
                           INNER JOIN CURSO c ON (c.Cod_Curso = ac.Cod_Curso)
      WHERE ac.Año=2016
      GROUP BY ac.Cod_Curso)
