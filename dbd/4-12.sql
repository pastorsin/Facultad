/*
* @Author: Andrés Milla
* @Date:   2018-10-25 16:46:00
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 16:49:19
*/
UPDATE PERSONA p
SET p.Estado_Civil = "Soltero"
WHERE p.DNI IN (SELECT *
                FROM ALUMNO a
                WHERE a.Legajo = "2020/9")
