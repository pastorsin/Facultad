/*
* @Author: Andrés Milla
* @Date:   2018-10-25 22:08:53
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 22:13:26
*/
SELECT DISTINCT(c.NombreCine, c.Ubicación)
FROM CINE c INNER JOIN FUNCION f ON (c.IdCine = f.IdCine)
            INNER JOIN PELICULA p ON (f.IdPelicula = p.IdPelicula)
            INNER JOIN ZONA z ON (c.IdZona = z.IdZona)
WHERE z.NombreZona = "Quilmes" and p.Título = "Temporada de Caza"
ORDER BY c.NombreCine
