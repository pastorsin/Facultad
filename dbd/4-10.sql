/*
* @Author: Andrés Milla
* @Date:   2018-10-25 16:13:54
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 16:20:03
*/
SELECT DISTINCT(p.Nombre, p.Apellido, p.Nombre, a.Legajo)
FROM ALUMNO-CURSO ac INNER JOIN ALUMNO a ON (a.DNI = ac.DNI)
                     INNER JOIN PERSONA p ON (a.DNI = p.DNI)
WHERE ac.Año = 2017 and a.DNI not IN (SELECT *
                                      FROM ALUMNO-CURSO ac INNER JOIN ALUMNO a ON (a.DNI = ac.DNI)
                                      WHERE ac.Año = 2016
                                     )
