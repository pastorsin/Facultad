/*
* @Author: Andrés Milla
* @Date:   2018-10-25 16:42:32
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 16:45:12
*/
INSERT INTO PERSONA (DNI, Apellido, Nombre, Fecha_Nacimiento, Estado_Civil, Genero)
VALUES (41206930, "Milla", "Andrés", "03-12-1998", "Soltero", "Masculino")

INSERT INTO PROFESOR (DNI, Matricula, Nro_Expediente)
VALUES (41206930, "14934/6", 4)

INSERT INTO TITULO-PROFESOR (Cod_Titulo, DNI, Fecha)
VALUES (25, 41206930, "25-10-2018")
