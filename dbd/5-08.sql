/*
* @Author: Andrés Milla
* @Date:   2018-10-26 15:56:30
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-26 15:57:35
*/
SELECT c.NombreCine, c.Ubicación
FROM CINE c INNER JOIN FUNCION f ON (c.IdCine = f.IdCine)
GROUP BY c.IdCine, c.NombreCine, c.Ubicación
HAVING COUNT(f.IdPelicula) > 3
