/*
* @Author: Andrés
* @Date:   2018-10-20 21:29:04
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:32:32
*/
SELECT Apellido, Nombre, Fecha_nacimiento, Fecha_ingreso
FROM SOCIO
WHERE Apellido = "Lopez" and Fecha_ingreso > STR_TO_DATE('10-01-2017', '%d-%m-%Y')
