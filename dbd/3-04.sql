/*
* @Author: Andrés
* @Date:   2018-10-20 21:35:00
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:37:01
*/
SELECT l.ISBN, l.Titulo, l.Descripcion
FROM LIBRO l INNER JOIN COPIA c ON (l.ISBN = c.ISBN)
WHERE c.Estado = "Regular"
