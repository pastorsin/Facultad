SELECT p.Dni, p.Nombre, p.Apellido, SUM(c.Duracion) as Horas totales, AVG(c.Duracion) as Promedio de horas
FROM PROFESOR pr INNER JOIN PERSONA p ON (pr.DNI = p.DNI)
                 INNER JOIN PROFESOR-CURSO pc ON (pr.DNI = pc.DNI)
                 INNER JOIN CURSO c ON (pc.Cod_Curso = c.Cod_Curso)
GROUP BY p.Dni, p.Nombre, p.Apellido
