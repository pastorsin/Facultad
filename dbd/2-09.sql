/*
* @Author: Andrés
* @Date:   2018-10-20 21:06:13
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:20:05
*/
SELECT f.nroTicket, f.total, f.fecha, f.hora
FROM DETALLE d  INNER JOIN FACTURA f  ON (d.nroTicket = f.nroTicket)
                INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
                INNER JOIN CLIENTE c  ON (f.idCliente = c.idCliente)
WHERE c.nombre = "Jorge" and c.apellido = "Perez" and p.nombreP = "Z"
