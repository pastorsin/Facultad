/*
* @Author: Andrés Milla
* @Date:   2018-10-22 20:11:41
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-22 20:26:09
*/
SELECT DISTINCT(s.DNI, s.Apellido, s.Nombre)
FROM SOCIO s    INNER JOIN PRESTAMO p ON (s.Cod_Socio = p.Cod_Socio)
                INNER JOIN LIBRO l ON (p.ISBN = l.ISBN)
                INNER JOIN GENERO g ON (l.Cod_Genero = g.Cod_Genero)
WHERE g.Nombre = "Novela" and p.Fecha_Prestamo IS NULL
