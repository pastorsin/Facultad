/*
* @Author: Andrés Milla
* @Date:   2018-10-22 20:53:34
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-22 20:54:35
*/
UPDATE LIBRO
SET Descripcion = "Una novela de intriga"
WHERE ISBN = "1235_4554"
