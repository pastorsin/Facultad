/*
* @Author: Andrés Milla
* @Date:   2018-10-25 22:20:00
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 22:24:22
*/
SELECT DISTINCT(c.Nombre, c.Ubicacion)
FROM FUNCION f INNER JOIN CINE c ON (f.IdCine = c.IdCine)
               INNER JOIN PELICULA p ON (p.IdPelicula = f.IdPelicula)
WHERE p.Titulo = "Logan" and c.IdCine not IN (  SELECT c.IdCine
                                                FROM FUNCION f INNER JOIN CINE c ON (f.IdCine = c.IdCine)
                                                               INNER JOIN PELICULA p ON (p.IdPelicula = f.IdPelicula)
                                                WHERE p.Nombre = "Liga de la Justicia"
                                             )
