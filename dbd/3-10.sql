/*
* @Author: Andrés Milla
* @Date:   2018-10-22 20:21:38
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-22 20:34:54
*/
SELECT COUNT(DISTINCT( s.Cod_Socio ))
FROM PRESTAMO p INNER JOIN LIBRO l ON (p.ISBN = l.ISBN)
                INNER JOIN COPIA c ON (p.ISBN = c.ISBN and p.Nro_Ejemplar = c.Nro_Ejemplar)
                INNER JOIN SOCIO s ON (s.Cod_Socio = p.Cod_Socio)
WHERE c.Estado = "Muy bueno"
