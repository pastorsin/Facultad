/*
* @Author: Andrés
* @Date:   2018-10-20 21:37:07
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:42:42
*/
SELECT DISTINCT(s.Apellido, s.Nombre, s.Fecha_Nacimiento)
FROM PRESTAMO p INNER JOIN SOCIO s ON (p.Cod_Socio = s.Cod_Socio)
                INNER JOIN LIBRO l ON (p.ISBN = l.ISBN)
                INNER JOIN GENERO g ON (l.Cod_Genero = g.Cod_Genero)
WHERE g.Nombre = "Novela" and s.CodSocio NOT IN (
    -- Los socios que pidieron préstamos de libros que no son de géneros novela
    SELECT s.CodSocio
    FROM PRESTAMO p INNER JOIN SOCIO s ON (p.Cod_Socio = s.Cod_Socio)
                    INNER JOIN LIBRO l ON (p.ISBN = l.ISBN)
                    INNER JOIN GENERO g ON (l.Cod_Genero = g.Cod_Genero)
    WHERE g.Nombre != "Novela"
)
