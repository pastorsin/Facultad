/*
* @Author: Andrés
* @Date:   2018-10-20 20:57:14
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:01:19
*/
SELECT p.nombreP, p.descripcion, p.precio, p.stock
FROM DETALLE d  INNER JOIN FACTURA f  ON (d.nroTicket = f.nroTicket)
                INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
                INNER JOIN CLIENTE c  ON (f.idCliente = c.idCliente)
WHERE c.dni = "258666239" and p.idProducto NOT IN (
    SELECT p.idProducto
    FROM DETALLE d  INNER JOIN FACTURA f  ON (d.nroTicket = f.nroTicket)
                    INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
                    INNER JOIN CLIENTE c  ON (f.idCliente = c.idCliente)
    WHERE c.dni = "2589633"
)
