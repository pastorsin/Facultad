/*
* @Author: Andrés
* @Date:   2018-10-20 18:50:33
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 20:37:50
*/
(SELECT c.nombre, c.apellido, c.dni, c.telefono, c.direccion
FROM DETALLE d INNER JOIN FACTURA f ON (f.nroTicket = d.nroTicket)
               INNER JOIN CLIENTE c ON (f.idCliente = c.idCliente)
               INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
WHERE p.nombreP = "Z")
INTERSECT
((SELECT c.nombre, c.apellido, c.dni, c.telefono, c.direccion
FROM CLIENTE c)
EXCEPT
(SELECT c.nombre, c.apellido, c.dni, c.telefono, c.direccion
FROM DETALLE d INNER JOIN FACTURA f ON (f.nroTicket = d.nroTicket)
               INNER JOIN CLIENTE c ON (f.idCliente = c.idCliente)
               INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
WHERE p.nombreP = "J" ))

-- En mysql

SELECT c.nombre, c.apellido, c.dni, c.telefono, c.direccion
FROM DETALLE d INNER JOIN FACTURA f ON (f.nroTicket = d.nroTicket)
               INNER JOIN CLIENTE c ON (f.idCliente = c.idCliente)
               INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
WHERE p.nombreP = "Z" and c.idCliente NOT IN ( -- Los que compraron "J"
        SELECT c.idCliente
        FROM DETALLE d INNER JOIN FACTURA f ON (f.nroTicket = d.nroTicket)
                       INNER JOIN CLIENTE c ON (f.idCliente = c.idCliente)
                       INNER JOIN PRODUCTO p ON (d.idProducto = p.idProducto)
        WHERE p.nombreP = "J"
    )
