/*
* @Author: Andrés
* @Date:   2018-10-20 21:48:03
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:51:30
*/
SELECT l.Titulo, g.Nombre
FROM LIBRO-EDITORIAL le INNER JOIN EDITORIAL e ON (le.Cod_Editorial = e.Cod_Editorial)
                        INNER JOIN LIBRO l ON (le.ISBN = l.ISBN)
                        INNER JOIN GENERO g ON (l.Cod_Genero = g.Cod_Genero)
WHERE e.Denominacion = "X" and le.Año_Edicion = 2017;
