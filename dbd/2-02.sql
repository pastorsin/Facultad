/*
* @Author: Andrés
* @Date:   2018-10-20 18:20:34
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 18:46:01
*/
(SELECT p.nombreP, p.descripcion, p.stock, p.precio
FROM PRODUCTO p)
EXCEPT
(SELECT p.nombreP, p.descripcion, p.stock, p.precio
FROM DETALLE d INNER JOIN PRODUCTO p ON (p.idProducto = d.idProducto))
ORDER BY p.nombreP

-- En mySQL:

SELECT p.nombreP, p.descripcion, p.stock, p.precio
FROM PRODUCTO p
WHERE p.idProducto NOT IN (SELECT p.idProducto
FROM DETALLE d INNER JOIN PRODUCTO p ON (p.idProducto = d.idProducto))
ORDER BY p.nombreP
