/*
* @Author: Andrés Milla
* @Date:   2018-10-22 20:56:29
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-22 21:04:22
*/
SELECT e.Denominacion, COUNT(c.ISBN)
FROM EDITORIAL e LEFT JOIN LIBRO-EDITORIAL le ON (e.Cod_Editorial = le.Cod_Editorial)
                 INNER JOIN COPIA c ON (le.ISBN = c.ISBN)
                 INNER JOIN PRESTAMO p ON (c.ISBN = p.ISBN)
GROUP BY e.Cod_Editorial
