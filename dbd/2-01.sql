/*
* @Author: Andrés
* @Date:   2018-10-20 18:20:15
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 18:48:00
*/
SELECT c.dni, c.nombre, c.apellido, SUM(f.total) as total
FROM FACTURA f INNER JOIN CLIENTE c ON (c.idCliente = f.idCliente)
GROUP BY c.idCliente, c.dni, c.nombre, c.apellido, f.total
HAVING SUM(f.total) > 200
