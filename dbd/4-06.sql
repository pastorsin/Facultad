SELECT p.DNI, p.Nombre, p.Apellido, pr.Matricula
FROM PROFESOR pr INNER JOIN PERSONA p ON (pr.DNI = p.DNI)
                 INNER JOIN TITULOS-PROFESOR tp ON (pr.DNI = tp.DNI)
                 INNER JOIN TITULOS t ON (t.Cod_Titulo = tp.Cod_Titulo)
GROUP BY p.DNI, p.Nombre, p.Apellido, pr.Matricula
HAVING COUNT(p.DNI) < 5
ORDER BY p.Apellido, p.Nombre
