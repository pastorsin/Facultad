/*
* @Author: Andrés Milla
* @Date:   2018-10-25 22:07:02
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 22:07:57
*/
SELECT z.NombreZona
FROM CINE c INNER JOIN ZONA z ON (c.IdZona = z.IdZona)
WHERE c.NombreCine = "Cinema La Plata"
