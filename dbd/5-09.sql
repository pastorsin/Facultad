/*
* @Author: Andrés Milla
* @Date:   2018-10-26 16:00:51
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-26 16:17:33
*/
SELECT p.Titulo, COUNT(DISTINCT(f.IdCine)) as CANTIDAD
FROM PELICULA p INNER JOIN FUNCION f (p.IdPelicula = f.IdPelicula)
GROUP BY p.IdPelicula, p.Título
ORDER BY CANTIDAD DESC
