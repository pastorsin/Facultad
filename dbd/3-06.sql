/*
* @Author: Andrés
* @Date:   2018-10-20 21:44:05
* @Last Modified by:   Andrés
* @Last Modified time: 2018-10-20 21:47:51
*/
SELECT Apellido, Nombre
FROM SOCIO
WHERE Fecha_Ingreso BETWEEN Date("01-01-2017") and Date("30/09/2017");
ORDER BY Apellido, Nombre
