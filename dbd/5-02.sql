/*
* @Author: Andrés Milla
* @Date:   2018-10-25 22:08:03
* @Last Modified by:   Andrés Milla
* @Last Modified time: 2018-10-25 22:08:48
*/
SELECT p.Título, p.Género, p.Duración
FROM PELICULA p
WHERE p.Año = 2017
